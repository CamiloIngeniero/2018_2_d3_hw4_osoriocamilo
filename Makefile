all: tx rx

tx: tx.cpp
	g++ -o tx tx.cpp -lrt
rx: rx.cpp
	g++ -o rx rx.cpp -lrt
	
.PHONY: clean
clean:
	rm -rf tx rx
